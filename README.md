# Initiation à SASS

## Qu'est ce que c'est ?
SASS est l'acronyme de `Syntactically Awesome Style Sheets`. C'est aussi, selon leur site :
>CSS with superpowers

>Sass is the most mature, stable, and powerful professional grade CSS extension
language in the world.

Donc en français, Sass est un preprocesseur CSS. Ca veut dire que Sass est un langage de
programmation prévu pour générer des fichers CSS.
 
C'est un langage compilé, ça veut donc dire
qu'il faut compiler les fichiers Sass (.scss) pour générer les fichiers CSS qui pourront être lus par le
navigateur. C'est son inconvénient comparé à son concurrent LESS utilisé par exemple par
Bootstrap qui lui est compilé par Javascript et peut donc être compilé à la volée. Pour le reste, ils
utilisent tous les deux une syntaxe assez proche. Sass étant plus complet, c'est ce langage que nous
allons voir, rien ne vous empêche de switcher sur LESS si le projet le demande.

## Installer Sass
Comme nous l'avons dit, Sass est un langage compilé, cela veut dire qu'il faut compiler nos fichiers
Sass pour générer les fichiers CSS. Pour compiler ces fichiers Sass utilise NodeJS, qu'il faut donc
installer. 
L'installation la plus aisée se fait avec le gestionnaire de dépendances npm.

### Ubuntu
- installer nvm (node version manager) en executant la commande `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash` ( pour les mises à jour voir ici : https://github.com/creationix/nvm)
- fermer et réouvrir le terminal
- installer la dernière version de node grâce à nvm : `nvm install node`
- maintenant que node et npm sont installés, on installe sass globalement avec `npm install -g sass`
- `sass --version` devrait nous retourner quelque chose.

### Windows
- installer nodeJS avec l'installeur disponile sur le site.
- Ouvrir un powershell ou un cmd et lancer la commande `npm install -g sass`
- `sass --version` devrait nous retourner quelque chose.

## Comment ça marche ?
Nous allons créer des fichiers .scss que nous allons compiler avec Sass. A la base on peut le faire en
ligne de commande : `sass input.scss output.css` ou pour « watcher » un répertoire en
entier : `sass --watch app/sass:public/stylesheets` qui va automatiquement
générer les fichiers .scss situés dans le répertoire app/sass et en faire des fichiers css dans le
repertoire public/stylesheets.
Pour faire plus simple nous allons utiliser une fonctionnalité de notre IDE et renseigner à ce dernier
l'endroit ou se trouve Sass. 

## Configuration de l'IDE

### Webstorm/IntelliJ
`File->Settings->Tools->File Watchers` cliquer sur le `+` et ajouter sass. Il faudra aussi lui indiquer l'endroit ou se trouve SASS. S'il n'est pas dans `/usr/bin` la commande `whereis sass` vous donnera son emplacement.

### Netbeans
`Tools->Options->Miscellaneous` et rester surl'onglet CSS Preprocessors. Dans le champ Saas Path nous allons browser vers un chemin du
genre : `/usr/bin/sass`.
Puis dans les propriétés de notre projet nous pourrons définir des répertoires cibles et de destination.
Afin de compiler automatiquement nos fichiers à la sauvegarde.


Pour le reste je vous emmène dans le [tutoriel de base](http://sass-lang.com/guide) et dans la [doc](http://sass-lang.com/documentation/file.SASS_REFERENCE.html) pour une petite visite des
fonctionnalités disponibles.

Vous trouverez dans ce dépôt un exemple des principales fonctionalités.